Loadout manager for Blacklight: Retribution, enabling users of BLRevive to use customization feature of BL:R.

# install

1. download the latest [Loadout Manager]() from the releases
2. move `LoadoutManager.dll` to `<BlrDir>\Binaries\Win32\Modules\LoadoutManager.dll`
3. enable the module in config (add `LoadoutManager` to client and server modules)
    ```json
    {
      "Proxy": {
        "Modules": {
          "Server": [..., "LoadoutManager"],
          "Client": [..., "LoadoutManager"]
        }
      }
    }
    ```

# use


## clients 

Players can define custom loadouts by providing a player profile.
If no such file is given, the module provides a default profile which is used instead.

### player profile
A player profile is a simple json file that lives inside `<BlrDir>\FoxGame\Config\BLRevive\profiles` and is named as `<playername>.json`.

If the player is logged in as `Homer`, the module expects a valid player profile at `<BlrDir>\FoxGame\Config\BLRevive\profiles\Homer.json` to exist.

After the file is parsed to a valid profile it is send to the server which overrides the loadout for the player natively.

### default profile

Servers can override the default loadout which is used by every connected player which did not define a loadout.

### config
