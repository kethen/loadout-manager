#pragma once

#include <string>
#include <map>
#include <vector>
#include <functional>
#include <mutex>
#include <nlohmann/json.hpp>

#define WIN32_LEAN_AND_MEAN
#include <winsock2.h>
#include <windows.h>
#include <Proxy/Events.h>
#include <Proxy/Network.h>

// forward declarations to avoid including sdk in header
class UClass;
class AFoxLoadoutInfo;
class UObject;
class UFunction;
class AFoxPC;
class AFoxPRI;
class UGameEngine;
struct FFoxWeaponConfigInfo;
class AFoxGame;
class UOnlineSubsystem;

namespace BLRevive::Loadout
{
	// forward declaration for PlayerLoadout (see implementation for more infos)
	class PlayerLoadout;
	// player profile definition (a collection of loadouts)
	using PlayerProfile = std::vector<PlayerLoadout>;
	// player profiles map
	using PlayerProfiles = std::map<AFoxPRI *, PlayerProfile>;

	/**
	 * Abstract loadout manager
	 */
	class LoadoutManager
	{
	public:
		// directory path of profile files
		inline static const std::string ProfileDirectory = Utils::FS::BlreviveConfigPath() + "\\profiles\\";

		/**
		 * Parse json to player profile
		 *
		 * @param  jPlayerProfile 	json
		 * @return PlayerProfile 	player profile
		 */
		PlayerProfile ParseProfileFromJson(json jPlayerProfile);

		/**
		 * Parse player profile from json file
		 *
		 * @param  ProfileName 		name of profile
		 * @return PlayerProfile 	player profile
		 */
		PlayerProfile ParseProfileFromFile(std::string ProfileName);

		LoadoutManager() { CheckDefaultFiles(); }

	protected:
		/**
		 * Check for default files and create them if not existant
		 */
		static void CheckDefaultFiles();
	};

	/**
	 * Loadout manager of server
	 */
	class ServerLoadoutManager : public LoadoutManager
	{
	public:
		/**
		 * Get profile of player (identified by PRI)
		 *
		 * @param  PRI 				PlayerReplicationInfo
		 * @return PlayerProfile 	player profile
		 */
		PlayerProfile GetPlayerProfile(AFoxPRI *PRI);

		/**
		 * Change the current profile of a player
		 *
		 * @param  PRI 			PlayerReplicationInfo
		 * @param  Profile 		player profile
		 */
		void SetPlayerProfile(AFoxPRI *PRI, PlayerProfile Profile);

		/**
		 * Overrides the native loadouts with custom player profiles
		 */
		void UpdateNativeProfiles();

		/**
		 * Register network handlers for server
		 *
		 * @param  server 	server interface
		 */
		void RegisterServerHandlers(std::shared_ptr<Network::Server> server);

		/**
		 * Initialize the loadout manager
		 */
		void Initialize();

		ServerLoadoutManager();

		// default profile of server
		PlayerProfile DefaultProfile;

	protected:
		// map of player profiles identified by AFoxPRI
		PlayerProfiles _Profiles;
		// map of selected profiles by player (needed to override the correct loadout)
		std::map<AFoxPRI *, int> _SelectedProfiles = std::map<AFoxPRI *, int>();

		/**
		 * Handle player login
		 *
		 * @param  PC 	player
		 */
		void PlayerLoginHandler(AFoxPC *PC);

		/**
		 * Handle player logout
		 *
		 * @param  PC 	player
		 */
		void PlayerLogoutHandler(AFoxPC *PC);

		/**
		 * Handle loadout swap of player
		 *
		 * @param  PC 					player
		 * @param  SelectedProfile 		selected profile slot
		 */
		void PlayerLoadoutSwapHandler(AFoxPC *PC, int SelectedProfile);

		/**
		 * Request handler for adding player profiles (PUSH ../profiles)
		 *
		 * @param  req 	request (needs valid json body)
		 * @param  res 	response (wether profile was added)
		 */
		void AddProfileRequestHandler(const httplib::Request &req, httplib::Response &res);

		/**
		 * Request handler for server default profile (GET ../profiles)
		 *
		 * @param  req 	request
		 * @param  res 	response
		 */
		void GetDefaultProfileRequestHandler(const httplib::Request &req, httplib::Response &res);
	};

	/**
	 * Loadout manager for clients
	 */
	class ClientLoadoutManager : public LoadoutManager
	{
	public:
		/**
		 * Get profile of current player
		 *
		 * @return PlayerProfile	player profile
		 */
		PlayerProfile GetProfile();

		/**
		 * Initialize loadout manager
		 */
		void Initialize();

		ClientLoadoutManager();

	protected:
		// current player
		AFoxPC *_Player;
		// current player profile
		PlayerProfile _Profile;

		/**
		 * Send the current profile to the server
		 */
		void SendProfile();

		/**
		 * Get default profile from server
		 *
		 * @return PlayerProfile 	default profile
		 */
		PlayerProfile GetServerDefaultProfile();

		/**
		 * Get current player
		 *
		 * @return AFoxPC* 	current player
		 */
		AFoxPC *GetCurrentPlayer();

		/**
		 * Get network address of Proxy's embbed server
		 *
		 * @return std::string 	blrevive network address
		 */
		std::string GetBlreviveNetworkAddress();
	};
}