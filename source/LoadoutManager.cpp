#include <LoadoutManager/LoadoutManager.h>

#pragma warning(disable : 4244)
#include <vector>
#include <LoadoutManager/Offsets.h>
#include <Proxy/Utils.h>
#include <LoadoutManager/resource.h>
#include <SdkHeaders.h>
#include <sys/stat.h>
#include <sys/types.h>
#include <filesystem>
using namespace BLRevive::Loadout;
namespace fs = std::filesystem;

namespace BLRevive::Loadout
{
	class PlayerLoadout
	{
	public:
		static AFoxLoadoutInfo* LoadoutInfo();
		static UFoxDataStore_Unlockables* UnlockablesInfo();

		PlayerLoadout() {}
		PlayerLoadout(FFoxWeaponConfigInfo Primary, FFoxWeaponConfigInfo Secondary, FProfileGearInfo Gear)
			: Primary(Primary), Secondary(Secondary), Gear(Gear) {}
		PlayerLoadout(nlohmann::json _j);

		const nlohmann::json ToJson();


		FFoxWeaponConfigInfo Primary;
		FFoxWeaponConfigInfo Secondary;
		FProfileGearInfo Gear;
		FFoxDepotPresetInfo Depot[5];
		// FProfileTauntInfo Taunts;

	protected:
		inline static AFoxLoadoutInfo* _LoadoutInfo = nullptr;
		inline static UFoxDataStore_Unlockables* _UnlockablesInfo = nullptr;
	};
}

#pragma region JSON converters
void to_json(json &j, const FFoxWeaponConfigInfo &config)
{
	j = json{
		{"Receiver", config.WeaponClass == NULL ? -1 : ((AFoxWeapon *)config.WeaponClass)->UnlockID},
		{"Ammo", config.WeaponAmmo == NULL ? -1 : ((UFoxWeaponAmmo_Base *)config.WeaponAmmo)->UnlockID},
		{"Barrel", config.WeaponBarrel == NULL ? -1 : ((UFoxWeaponBarrel *)config.WeaponBarrel)->UnlockID},
		{"Scope", config.WeaponScope == NULL ? -1 : ((UFoxWeaponScope *)config.WeaponScope)->UnlockID},
		{"Muzzle", (int)config.WeaponMuzzle},
		{"Grip", config.WeaponGrip == NULL ? -1 : ((UFoxWeaponGrip *)config.WeaponGrip)->UnlockID},
		{"Magazine", (int)config.WeaponMagazine},
		{"Stock", config.WeaponStock == NULL ? -1 : ((UFoxWeaponStock *)config.WeaponStock)->UnlockID},
		{"CamoIndex", (int)config.WeaponCamoIndex},
		{"Skin", (int)config.WeaponSkin},
		{"Hanger", (int)config.WeaponHanger}};
}

void from_json(const json &j, FFoxWeaponConfigInfo &config)
{
	AFoxLoadoutInfo *LoadoutInfo = PlayerLoadout::LoadoutInfo();
	UFoxDataStore_Unlockables *UDS = PlayerLoadout::UnlockablesInfo();

	if (!LoadoutInfo || !UDS)
	{
		LError("Failed to parse weapon config because provider is missing.");
		return;
	}

	auto GetWeaponsProviderById = [=]<typename T>(int UID, TArray<T *> arr)
	{
		for (int i = 0; i < arr.Count; i++)
		{
			if (arr.Data[0] && arr.Data[0]->IsA(UFoxDataProvider_Unlockable::StaticClass()) && ((UFoxDataProvider_Unlockable *)arr.Data[i])->UnlockID == UID)
				return i;
		}
		return 0;
	};

	config.WeaponClass = LoadoutInfo->GetWeaponClassById((int)j["Receiver"]);
	config.WeaponBarrel = LoadoutInfo->eventGetModClassById((int)j["Barrel"], UT_Barrel);
	config.WeaponScope = LoadoutInfo->eventGetModClassById((int)j["Scope"], UT_Scope);
	config.WeaponGrip = LoadoutInfo->eventGetModClassById((int)j["Grip"], UT_Grip);
	config.WeaponStock = LoadoutInfo->eventGetModClassById((int)j["Stock"], UT_Stock);
	config.WeaponAmmo = LoadoutInfo->eventGetModClassById((int)j["Ammo"], UT_Ammo);

	config.WeaponMuzzle = GetWeaponsProviderById((int)j["Muzzle"], UDS->MuzzleProviderArray);
	config.WeaponMagazine = GetWeaponsProviderById((int)j["Magazine"], UDS->MagazineProviderArray);
	config.WeaponCamoIndex = (int)j["CamoIndex"];
	config.WeaponHanger = GetWeaponsProviderById((int)j["Hanger"], UDS->HangerProviderArray);
}

void to_json(json &j, const FProfileGearInfo &gear)
{
	j = {
		{"Female", gear.bFemale},
		{"Bot", gear.bBot},
		{"BodyCamo", gear.BodyCamoID},
		{"UpperBody", gear.UpperBodyID},
		{"LowerBody", gear.LowerBodyID},
		{"Helmet", gear.HelmetID},
		{"Badge", gear.BadgeID},
		{"Gear_R1", gear.Gear_R1ID},
		{"Gear_R2", gear.Gear_R2ID},
		{"Gear_L1", gear.Gear_L1ID},
		{"Gear_L2", gear.Gear_L2ID},
		{"Tactical", gear.TacticalID},
		{"ButtPack", gear.ButtPackID},
		{"Avatar", gear.AvatarID},
		{"PatchIcon", gear.PatchIconID},
		{"PatchIconColor", gear.PatchIconColorID},
		{"PatchShape", gear.PatchShapeID},
		{"PatchShapeColor", gear.PatchShapeColorID},
		{"Hanger", gear.HangerID}};
}

void from_json(const json &j, FProfileGearInfo &gear)
{
	gear.bFemale = j["Female"].get<bool>();
	gear.bBot = j["Bot"].get<bool>();
	j.at("BodyCamo").get_to(gear.BodyCamoID);
	j.at("UpperBody").get_to(gear.UpperBodyID);
	j.at("LowerBody").get_to(gear.LowerBodyID);
	j.at("Helmet").get_to(gear.HelmetID);
	j.at("Badge").get_to(gear.BadgeID);
	j.at("Gear_R1").get_to(gear.Gear_R1ID);
	j.at("Gear_R2").get_to(gear.Gear_R2ID);
	j.at("Gear_L1").get_to(gear.Gear_L1ID);
	j.at("Gear_L2").get_to(gear.Gear_L2ID);
	j.at("Tactical").get_to(gear.TacticalID);
	j.at("ButtPack").get_to(gear.ButtPackID);
	j.at("Avatar").get_to(gear.AvatarID);
	j.at("PatchIcon").get_to(gear.PatchIconID);
	j.at("PatchIconColor").get_to(gear.PatchIconColorID);
	j.at("PatchShape").get_to(gear.PatchShapeID);
	j.at("PatchShapeColor").get_to(gear.PatchShapeColorID);
	j.at("Hanger").get_to(gear.HangerID);
}
#pragma endregion

namespace BLRevive::Loadout
{
	PlayerLoadout::PlayerLoadout(nlohmann::json _j)
	{
		_j.at("Primary").get_to(Primary);
		_j.at("Secondary").get_to(Secondary);
		_j.at("Gear").get_to(Gear);

		for (int i = 0; i <= 4; i++)
			Depot[i] = LoadoutInfo()->GetDepotPresetFromIndex((int)_j["Depot"][i]);
	}

	const nlohmann::json PlayerLoadout::ToJson()
	{
		const nlohmann::json l{
			{"Primary", Primary},
			{"Secondary", Secondary},
			{"Gear", Gear},
			{"Depot", {
				Depot[0].UnlockID,
				Depot[1].UnlockID,
				Depot[2].UnlockID,
				Depot[3].UnlockID,
				Depot[4].UnlockID
			}}
		};
		return l;
	}

	AFoxLoadoutInfo *PlayerLoadout::LoadoutInfo()
	{
		if (!_LoadoutInfo)
		{
			_LoadoutInfo = UObject::GetInstanceOf<AFoxLoadoutInfo>(true);
			if (!_LoadoutInfo)
			{
				LError("Unable to get instance of AFoxLoadoutInfo");
				return nullptr;
			}
		}
		return _LoadoutInfo;
	}

	UFoxDataStore_Unlockables *PlayerLoadout::UnlockablesInfo()
	{
		if (!_UnlockablesInfo)
		{
			_UnlockablesInfo = UObject::GetInstanceOf<UFoxDataStore_Unlockables>(true);
			if (!_UnlockablesInfo)
			{
				LError("Unable to get instance of UFoxDataStore_Unlockables");
				return nullptr;
			}
		}

		return _UnlockablesInfo;
	}

}

#pragma region LoadoutManager
PlayerProfile LoadoutManager::ParseProfileFromFile(std::string ProfileName)
{
	// open profile file
	std::ifstream file(ProfileDirectory + ProfileName + ".json");
	if (file.good())
	{
		// parse file to json
		json profile;
		file >> profile;
		file.close();
		// parse json to player profile
		return ParseProfileFromJson(profile);
	}

	return PlayerProfile();
}

PlayerProfile LoadoutManager::ParseProfileFromJson(json j)
{
	try
	{
		// parse json profile to class
		auto profile = PlayerProfile();
		for (auto &jloadout : j)
			profile.push_back(PlayerLoadout(jloadout));
		return profile;
	}
	catch (json::exception e)
	{
		LError("Failed to parse player loadout from json: {}", e.what());
		return PlayerProfile();
	}
}

void LoadoutManager::CheckDefaultFiles()
{
	struct stat Info;
	if (stat(ProfileDirectory.c_str(), &Info) != 0)
	{
		fs::create_directory(ProfileDirectory.c_str());
	}

	if (stat((ProfileDirectory + "default.json").c_str(), &Info))
	{
		std::ofstream def(ProfileDirectory + "default.json");
		def << Resources::Get<std::string>(RES_LOADOUT_DEFAULT);
		def.close();
	}
}
#pragma endregion

#pragma region ServerLoadoutManager
ServerLoadoutManager::ServerLoadoutManager() : LoadoutManager()
{
	DefaultProfile = ParseProfileFromFile(Utils::URL::Param::String("Profile", "default"));
}

void ServerLoadoutManager::Initialize()
{
	auto mgr = Events::Manager::GetInstance();

	mgr->RegisterHandler({Events::ID("FoxPC", "ServerSetLoadout"),
						  [=](Events::Info Info)
						  {
							  auto PC = (AFoxPC *)Info.Object;
							  auto Params = (AFoxPC_execServerSetLoadout_Parms *)Info.Params;
							  this->PlayerLoadoutSwapHandler(PC, Params->LoadoutData.LoadoutIndex);
						  }});

	mgr->RegisterHandler({Events::ID("*", "PostLogin"),
						  [=](Events::Info Info)
						  {
							  auto Params = (AFoxGame_eventPostLogin_Parms *)Info.Params;
							  this->PlayerLoginHandler((AFoxPC *)Params->NewPlayer);
						  }});

	mgr->RegisterHandler({Events::ID("FoxGame", "Logout"),
						  [=](Events::Info Info)
						  {
							  auto Params = (AFoxGame_execLogout_Parms *)Info.Params;
							  this->PlayerLogoutHandler((AFoxPC *)Params->Exiting);
						  }});
}

void ServerLoadoutManager::RegisterServerHandlers(std::shared_ptr<Network::Server> server)
{
	server->AddConnectionHandler(Network::RequestType::POST, "/loadouts", [&](const httplib::Request &req, httplib::Response &res)
								 { this->AddProfileRequestHandler(req, res); });
	server->AddConnectionHandler(Network::RequestType::GET, "/loadouts", [&](const httplib::Request &req, httplib::Response &res)
								 { this->GetDefaultProfileRequestHandler(req, res); });
}

PlayerProfile ServerLoadoutManager::GetPlayerProfile(AFoxPRI *PRI)
{
	return _Profiles[PRI];
}

void ServerLoadoutManager::SetPlayerProfile(AFoxPRI *PRI, PlayerProfile Profile)
{
	_Profiles[PRI] = Profile;
}

void ServerLoadoutManager::AddProfileRequestHandler(const httplib::Request &req, httplib::Response &res)
{
	// check if player is in match (AFoxPC exists)
	for (auto &[player, oldProfile] : _Profiles)
	{
		if (player->SavedNetworkAddress.ToChar() == req.remote_addr)
		{
			try
			{
				// validate and parse player loadouts
				auto profile = PlayerProfile();
				json jbody = json::parse(req.body);
				for (json loadout : jbody)
					profile.push_back(PlayerLoadout(loadout));

				this->SetPlayerProfile(player, profile);
				return;
			}
			catch (json::exception e)
			{
				LError("{}", e.what());
			}
		}
	}

	res.status = 401;
	res.set_content("Access denied!", "text/plain");
}

void ServerLoadoutManager::GetDefaultProfileRequestHandler(const httplib::Request &req, httplib::Response &res)
{
	std::ifstream def(ProfileDirectory + "\\default.json");
	if (def.good())
	{
		std::stringstream sdef;
		sdef << def.rdbuf();

		res.set_content(sdef.str(), "application/json");
	}
}

void ServerLoadoutManager::PlayerLoadoutSwapHandler(AFoxPC *PC, int LoadoutIndex)
{
	LDebug("Player swap loadout for {0} to {1}", PC->PlayerReplicationInfo->PlayerName.ToChar(), LoadoutIndex);
	_SelectedProfiles[(AFoxPRI *)PC->PlayerReplicationInfo] = LoadoutIndex;
}

void ServerLoadoutManager::PlayerLoginHandler(AFoxPC *PC)
{
	LDebug("LoadoutManager::PlayerLoginHandler {0}", PC->PlayerReplicationInfo->PlayerName.ToChar());
	LFlush;
	auto pri = (AFoxPRI *)PC->PlayerReplicationInfo;
	_Profiles[pri] = DefaultProfile;
	_SelectedProfiles[pri] = 0;
}

void ServerLoadoutManager::PlayerLogoutHandler(AFoxPC *PC)
{
	_Profiles.erase((AFoxPRI *)PC->PlayerReplicationInfo);
}

void ServerLoadoutManager::UpdateNativeProfiles()
{
	static AFoxGRI* gri = UObject::GetInstanceOf<AFoxGRI>();
	static AFoxPC* localPc = gri->GetTheLocalPlayerController();

	for (auto [PC, Loadouts] : _Profiles)
	{
		if (PC == NULL || PC->Loadout == NULL)
			continue;

		int selectedSlot = _SelectedProfiles[PC];
		if (selectedSlot >= 0 && Loadouts.size() > 0 && selectedSlot < Loadouts.size())
		{
			auto &cachedLoadout = Loadouts[selectedSlot];
			PC->Loadout->PrimaryWeaponInfo = cachedLoadout.Primary;
			PC->Loadout->SecondaryWeaponInfo = cachedLoadout.Secondary;
			PC->Loadout->GearInfo = cachedLoadout.Gear;

			/*for (int i = 0; i <= 4; i++)
				PC->Loadout->DepotItems[i] = cachedLoadout.Depot[i];*/
		}
		else
		{
			LError("No loadout for at index {0}", selectedSlot);
		}
	}
}
#pragma endregion

#pragma region ClientLoadoutManager
ClientLoadoutManager::ClientLoadoutManager() : LoadoutManager()
{
}

void ClientLoadoutManager::Initialize()
{
	// load player loadout from json
	AFoxPC *pc = GetCurrentPlayer();
	while (pc == NULL)
	{
		pc = GetCurrentPlayer();
		Sleep(500);
	}

	((UOnlineSubsystemPW*)pc->OnlineSub)->LoggedInStatus = 2;
	auto storeData = UObject::GetInstanceOf<UFoxDataStore_StoreData>(true);
	auto invCache = storeData->InventoryCache;

	invCache->bIsDirty = false;
	invCache->ReadInventoryComplete(pc->PlayerReplicationInfo->UniqueId, true, FString(""));

	_Player = pc;
	std::string playername = pc->PlayerReplicationInfo->PlayerName.ToChar();
	if (playername == "Player")
		playername = "default";

	_Profile = ParseProfileFromFile(playername);
	if (!_Profile.empty())
	{
		SendProfile();
	}
	else
	{
		_Profile = GetServerDefaultProfile();
	}

	/*auto eventMgr = Events::Manager::GetInstance();
	eventMgr->RegisterHandler({{"FoxLoadoutSwapperWidget", "ei_loadoutRollover"},
							   [&](Events::Info data)
							   {
								   auto lSwapper = (UFoxLoadoutSwapperWidget *)data.Object;
								   auto parms = (UFoxLoadoutSwapperWidget_execei_loadoutRollover_Parms *)data.Params;

								   TArray<FLoadoutInfo> loadoutData;

								   loadoutData.Add(FLoadoutInfo
									   {
										   .sPrimaryHeader = "PHeader",
										   .sSecondaryHeader = "SHeader",
										   .sPrimaryName = "PName",
										   .sSecondaryName = "SName",
										   .sDefaultInfo = "Default info",
										   .sLoadoutNumber = "Loadout Number"
									   }
								   );

								   lSwapper->as_loadoutInfo(loadoutData);

								   LDebug("{}->{}", data.Object->Name.GetName(), data.Function->Name.GetName());
							   },
							   false,
							   false});*/
}

PlayerProfile ClientLoadoutManager::GetProfile()
{
	return _Profile;
}

AFoxPC *ClientLoadoutManager::GetCurrentPlayer()
{
	static AFoxPC *pc = NULL;
	if (pc == NULL)
	{
		auto apc = UObject::GetInstanceOf<AFoxPC>();
		if (apc != NULL && apc->PlayerReplicationInfo && std::string(apc->PlayerReplicationInfo->PlayerName.ToChar()) != "Player")
			pc = apc;
	}
	return pc;
}

void ClientLoadoutManager::SendProfile()
{
	std::ifstream profileFile(ProfileDirectory + "\\" + ((AFoxPRI *)_Player->PlayerReplicationInfo)->PlayerName.ToChar() + ".json");
	if (!profileFile.good())
	{
		profileFile.close();
		return;
	}
	std::stringstream buff;
	buff << profileFile.rdbuf();

	httplib::Client cli(GetBlreviveNetworkAddress());
	auto res = cli.Post("/loadouts", buff.str(), "application/json");
}

PlayerProfile ClientLoadoutManager::GetServerDefaultProfile()
{
	httplib::Client cli(GetBlreviveNetworkAddress());
	auto res = cli.Get("/loadouts");
	auto j = json::parse(res.value().body);
	PlayerProfile p;
	for (auto jloadout : j)
		p.push_back(PlayerLoadout(jloadout));
	return p;
}

std::string ClientLoadoutManager::GetBlreviveNetworkAddress()
{
	static std::string BlrRvAddress;

	if (BlrRvAddress.empty())
	{
		std::string host = _Player->GetServerNetworkAddress().ToChar();
		int BlrPort = Utils::URL::Param::Int("Port", 7777);
		int BlrRvPort = Utils::URL::Param::Int("RvPort", BlrPort + 1);
		BlrRvAddress = host + ":" + std::to_string(BlrRvPort);
	}

	return BlrRvAddress;
}
#pragma endregion