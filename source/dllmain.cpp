// dllmain.cpp : Defines the entry point for the DLL application.
#include <LoadoutManager/LoadoutManager.h>

// for access to event manager
#include <Proxy/Events.h>
// for access to client/server
#include <Proxy/Network.h>
// for module api
#include <Proxy/Modules.h>
// for logging in main file
#include <Proxy/Logger.h>
using namespace BLRevive;

static Loadout::LoadoutManager *_LM = NULL;

/**
 * Thread thats specific to the module (function must exist and export demangled!)
 */
extern "C" __declspec(dllexport) void ModuleThread()
{
    if (Utils::IsServer())
    {
        auto lm = (Loadout::ServerLoadoutManager *)_LM;
        lm->Initialize();
        while (true)
        {
            lm->UpdateNativeProfiles();
            Sleep(1000);
        }
    }
    else
    {
        auto lm = (Loadout::ClientLoadoutManager *)_LM;
        lm->Initialize();
    }
}

/**
 * Module initializer (function must exist and exported demangled!)
 */
extern "C" __declspec(dllexport) void InitializeModule(Module::InitData *data)
{
    // check param validity
    if (!data->EventManager || !data->Logger)
    {
        LError("module initializer param was null!");
        LFlush;
        return;
    }

    // initialize logger (to enable logging to the same file)
    Logger::Link(data->Logger);
    LDebug("Logger initialized!");
    // initialize event manager
    // an instance of the manager can be retrieved with Events::Manager::Instance() afterwards
    Events::Manager::Link(data->EventManager);

    if (Utils::IsServer())
    {
        auto lm = new Loadout::ServerLoadoutManager();
        lm->RegisterServerHandlers(data->Server);
        _LM = lm;
    }
    else
    {
        auto lm = new Loadout::ClientLoadoutManager();
        _LM = lm;
    }
}

BOOL APIENTRY DllMain(HMODULE hModule,
                      DWORD ul_reason_for_call,
                      LPVOID lpReserved)
{
    switch (ul_reason_for_call)
    {
    case DLL_PROCESS_ATTACH:
    case DLL_THREAD_ATTACH:
    case DLL_THREAD_DETACH:
    case DLL_PROCESS_DETACH:
        break;
    }
    return TRUE;
}
